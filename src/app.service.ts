import { Injectable, Logger } from '@nestjs/common';

@Injectable()
export class AppService {
  logger:Logger;

  constructor(){
    this.logger = new Logger();
    //this.logger = new Logger(AppService.name);
  }

  getHello(): string {
    this.logger.log('test');
   // this.logger.debug('test');
    return 'Hello World!';
  }
}
